import axios from 'axios'

let moment = require('moment')
moment.locale('bg')

let mix = {
  methods: {
    ajaxCall (method, url, params = '', alert = true) {
      if (!url || !method) {
        console.log('Please read the instruction')
        return 'Please read the instruction'
      }
      let config = {
        'method': method,
        'url': url,
        'maxRedirects': 2
      }
      config['headers'] = {
        'Content-Type': 'application/json',
        'charset': 'UTF-8',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': ' Bearer ' + localStorage.access_token,
        'Access-Control-Allow-Origin': '*'
      }
      if ((method === 'POST')) {
        if (!params) {
          console.log('POST without body')
          return 'POST without body'
        } else {
          config['data'] = params
        }
      }
      if ((method === 'PATCH')) {
        if (!params) {
          console.log('POST without body')
          return 'POST without body'
        } else {
          config['data'] = params
        }
      }
      if ((method === 'GET')) {
        if (params) {
          config['params'] = params
        }
      }
      if ((method === 'DELETE')) {
        if (params) {
          config['data'] = params
        }
      }

      return axios(config)
        .then((response) => {
          try {
            if (response.data !== '') {
              JSON.parse(response.data)
            }
          } catch (error) {
            if (typeof response.data === 'string') {
              console.error('Result is not JSON. ' + error.message + ' response: ' + response.data)
            }
          }
          return new Promise((resolve, reject) => {
            resolve(response.data)
          })
        })
        .catch((err) => {
          return new Promise((resolve, reject) => {
            if (err.response.status === 401) {
              if (alert) {
                this.showAlert('Сесията ви изтече. Влезте наново', true)
                setTimeout(() => {
                  this.$router.push('/')
                }, 2000)
              }
            } else if (err.response.status === 403) {
              this.$router.push('/error/403')
            } else if (err.response.status === 500) {
              this.showAlert('Възникна грешка', true)
            }
            reject(err)
          })
        })
    },
    get (url, alert = true) {
      return this.ajaxCall('GET', url)
    },
    post (url, params, alert = true) {
      return this.ajaxCall('POST', url, params, alert)
    },
    patch (url, params) {
      return this.ajaxCall('PATCH', url, params)
    },
    delete (url, params) {
      return this.ajaxCall('DELETE', url, params)
    },
    redirect (url) {
      window.location.replace(url)
    },
    redirectWithRouter (url) {
      this.$router.push(url)
    },
    convertDate (date, format = 'DD-MM-YYYY') {
      if (date !== '') {
        let d = moment(date).format(format)
        return d
      }
      return ''
    },
    showAlert (text, error = false) {
      let alert = {
        error: error,
        show: true,
        text: text
      }
      this.$EventBus.$emit('alert', alert)
    },
    moment (val) {
      return moment(val)
    },
    showDialog (data = {}) {
      this.$EventBus.$emit('show-dialog', data)
    },
    closeDialog (data = {}) {
      this.$EventBus.$emit('close-dialog', data)
    },
    showPopUp (text) {
      let alert = {
        snackbar: true,
        text: text
      }
      this.$EventBus.$emit('popup', alert)
    },
    clear () {
      this.$EventBus.$emit('clear-all')
    },
    setTitle (title) {
      this.$EventBus.$emit('setTitle', title)
    },
    routeIs (route) {
      return this.$route.name === route
    },
    routeIsNot (route) {
      return this.$route.name !== route
    },
    startLoading () {
      this.$EventBus.$emit('loading')
    },
    stopLoading () {
      this.$EventBus.$emit('stop-loading')
    },
    push_notification (img_url = '', body, url) {
      Notification.requestPermission(permission => {
      })
      if (Notification.permission === 'granted') {
        navigator.serviceWorker.ready.then(function (reg) {
          if (reg === undefined) {
            console.log('only works online')
            return
          }
          let notification = {
            body: body, // content for the alert
            icon: 'https://pusher.com/static_logos/320x320.png' // optional image url
          }

          // link to page on clicking the notification
          reg.showNotification('Your Message Here!', notification)
          notification.onclick = () => {
            window.open(url)
          }
        }
        )
      }
    }
  }
}

export default mix
