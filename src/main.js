// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import mix from './mixins/mixin'
import store from './store/store'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import i18n from './plugins/i18n'
import router from './router/router'

i18n.locale = 'en'

Vue.use(VueAwesomeSwiper)

axios.defaults.baseURL = 'https://booking.localhost/api'
Vue.prototype.$domain = 'https://booking.localhost/'
Vue.mixin(mix)

Vue.prototype.$EventBus = new Vue()

Vue.config.productionTip = false
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBsD-YxVTr2QDGl-CJjS7i_3dbhv29rZls',
    libraries: 'places' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
    // If you want to set the version, you can do so:
    // v: '3.26',
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  router: router(),
  vuetify,
  store,
  template: '<App/>',
  components: {App}
})
