// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: '#AD974F', //  //  #d4a26f
        secondary: '#8E793E',
        background: '#231F20'
      },
      light: {
        primary: '#AD974F',
        secondary: '#8E793E',
        background: '#EAEAEA'
      }
    }
  }
}

export default new Vuetify(opts)
