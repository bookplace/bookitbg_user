import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {subdomain: true, section: 0}
    },
    {
      path: '/reservations',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {subdomain: true, section: 1}

    },
    {
      path: '/events',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {subdomain: true, section: 2}

    },
    {
      path: '/events/:id',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {subdomain: true, section: 2, default: true}
    },

    {
      path: '/photos',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {subdomain: true, section: 3}

    }
  ]
})
