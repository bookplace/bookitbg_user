import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Home/Home')
    },
    {
      path: '/auth',
      name: 'auth',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Auth/AuthPage'),
      children: [
        {
          path: '/',
          name: 'auth',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Auth/Auth')
          }
        },
        {
          path: 'login',
          name: 'login',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Auth/Login')
          }
        },
        {
          path: 'register',
          name: 'register',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Auth/Register')
          }
        }
      ]
    },
    {
      path: '/profile/',
      name: 'Profile',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Profile/ProfileView'),
      children: [
        {
          path: '/profile/',
          name: 'Profile',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Profile/Profile')
          }
        },
        {
          path: '/profile/orders',
          name: 'Profile',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Profile/Orders')
          }
        },
        {
          path: '/profile/reservations',
          name: 'Profile',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Profile/Reservations')
          }
        },
        {
          path: '/profile/favourites',
          name: 'Profile',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Profile/Favourites')
          }
        }
      ]
    },
    {
      path: '/:place_id/reservations',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {section: 1}

    },
    {
      path: '/:place_id/events',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {section: 2}

    },
    {
      path: '/:place_id/events/:id',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {section: 2, default: true}
    },

    {
      path: '/:place_id/photos',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {section: 3}

    },
    {
      path: '/:place_id',
      name: 'places',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {section: 0}
    },
    {
      path: '/:place_id/menu/',
      name: 'menu',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Place/Place'),
      props: {section: 4},
      children: [
        {
          path: '/',
          name: 'menu_categories',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Menu/Categories')
          }
        },
        {
          path: ':category_id/:subcategory_id',
          name: 'menu_subcategories',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Menu/Products/Products')
          }
        },
        {
          path: ':product_id',
          name: 'menu_products',
          components: {
            b: () => import(/* webpackChunkName: "group-foo" */ '../components/Menu/Products/ProductInfo')
          }
        }
      ]
    },
    {
      path: '/:place_id/orders/create',
      name: 'create_order',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Orders/CreateOrder'),
      props: true

    },
    {
      path: '/:place_id/orders/confirm',
      name: 'confirm_order',
      component: () => import(/* webpackChunkName: "group-foo" */ '../components/Orders/ClientDetails'),
      props: true

    }
  ]
})
