import index from './index'
import placeRouter from './placeRouter'
const host = window.location.host
const parts = host.split('.')
const domainLength = 3

const router = () => {
  let routes
  if (parts.length === (domainLength - 1) || parts[0] === 'www') {
    routes = index
  } else {
    // If you want to do something else just comment the line below
    routes = placeRouter
  }
  return routes
}

export default router
