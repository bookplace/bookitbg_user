import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    distance: 0,
    filters: {
      city: '*',
      search: ' ',
      type: 0
    },
    user: null,
    subcategory: 0
  },
  mutations: {
    setDistance (state, distance) {
      state.distance = distance
    },
    setFilters (state, filters) {
      state.filters = filters
    },
    setSubcategory (state, subcategory) {
      state.subcategory = subcategory
    },
    setUser (state, user) {
      state.user = user
    }
  },
  getters: {
    getDistance: state => {
      return state.distance
    },
    getFilters: state => {
      return state.filters
    },
    getSubcategory: state => {
      return state.subcategory
    },
    getUser: state => {
      return state.user
    }

  }
})

export default store
